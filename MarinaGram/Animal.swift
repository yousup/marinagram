//
//  Exhibit.swift
//  Aquide
//
//  Created by Yousup Lee on 11/22/14.
//  Copyright (c) 2014 Yousup Lee. All rights reserved.
//

import UIKit

@objc
class Animal {
    let name: String
    let scientificName: String
    let image: String
    let tidbit: String
    
    private struct animalStruct {
        static let baseUrl: String = "http://Aquide-vitxpp35pz.elasticbeanstalk.com/api/animal/"
//        static let baseUrl: String = "http://localhost:3000/api/animal/"
        static var aquariumCache: Array<Animal> = []
        static var lastAnimalExhibit: String = ""
        static var lastAnimalAquarium: String = ""
        static var animalThumbnailCache: NSCache = NSCache()
    }
    
    init(name: String, image: String, scientificName: String, tidbit: String){
        self.name = name
        self.image = image
        self.scientificName = scientificName
        self.tidbit = tidbit
    }
    
    class func animalsForAquarium(aquariumName: String) -> Array<Animal>? {
        var result: Array<Animal> = []
        if (animalStruct.aquariumCache.isEmpty || aquariumName != animalStruct.lastAnimalAquarium){
            let jsonDictionary = HttpHandler.getJSON(animalStruct.baseUrl, query: aquariumName+"/aquarium")
            if jsonDictionary != nil {
                for value in jsonDictionary!.allValues {
                    result.append(Animal(name: value["name"]! as! String, image: value["image"]! as! String, scientificName: value["scientificName"]! as! String, tidbit: value["tidbit"]! as! String))
                }
                animalStruct.aquariumCache = result
                animalStruct.lastAnimalAquarium = aquariumName
            }
            else {
                return nil
            }
        }
        else {
            return animalStruct.aquariumCache
        }
        return result
    }
    
    class func getAnimalImage(imageName: String, inout image: UIImageView!) {
        HttpHandler.getImageAsync("http://Aquide-vitxpp35pz.elasticbeanstalk.com/image/animal/", query: imageName, image: &image)
    }
    
    class func getThumbnailImage(imageName: String, inout image: UIImageView!) {
        image.image = animalStruct.animalThumbnailCache.objectForKey(imageName) as? UIImage
        if ((image.image) != nil) {
            // Nothing to do
        }
        else {
            HttpHandler.getImageAsync("http://Aquide-vitxpp35pz.elasticbeanstalk.com/thumbnailimage/animal/", query: imageName, image: &image, cache: &animalStruct.animalThumbnailCache)
        }
    }
}
