//
//  AquariumViewController.swift
//  MarinaGram
//
//  Created by Yousup Lee on 3/21/15.
//  Copyright (c) 2015 Yousup Lee. All rights reserved.
//

import UIKit

class AquariumViewController: UIViewController {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var refreshButton: UIButton!
    var animals: [Animal]!
    var initCache = []
    
    struct TableView {
        struct AnimalCellIdentifiers {
            static let AnimalCell = "AnimalCell"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collection.allowsMultipleSelection = false
        name.text = initCache[0] as? String
        animals = Animal.animalsForAquarium(name.text!)
        if let a = animals {
            animals.sort({ $0.name < $1.name })
            errorLabel.hidden = true
            refreshButton.hidden = true
        }
        else {
            errorLabel.hidden = false
            refreshButton.hidden = false
        }
        collection.reloadData()
        self.title = name.text
    }
    
    override func viewWillAppear(animated: Bool) {
        self.title = name.text
    }
    
    override func viewWillLayoutSubviews() {
        // Resize the collection cells on rotation
        if let a = collection {
            self.collection.collectionViewLayout.invalidateLayout()
        }
        
        super.viewWillLayoutSubviews()
    }
    
    @IBAction func refreshPressed() {
        viewDidLoad()
        viewWillAppear(false)
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let a = animals {
            return animals.count
        }
        else {
            return 0
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(TableView.AnimalCellIdentifiers.AnimalCell, forIndexPath: indexPath) as! AnimalCell
        
        cell.configureForAnimal(animals[indexPath.row])
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        var width: CGFloat = UIScreen.mainScreen().bounds.size.width as CGFloat!
        width = width - 20
        
        if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation))
        {
            return CGSize(width: (width/3)-(20/3), height: (width/3)-(20/3))
        }
        
        return CGSize(width: (width/2)-5, height: (width/2)-5)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!){
        if segue.identifier == "aquariumToAnimal" {
            let vc = segue.destinationViewController as! AnimalViewController
            //            vc.hidesBottomBarWhenPushed = true
            var selectedAnimal: Animal!
            let cell = sender as! AnimalCell
            let indexPath = collection.indexPathForCell(cell)
            selectedAnimal = animals[indexPath!.row]
            self.navigationItem.title = nil
            if let i = vc.name {
                vc.name.text = selectedAnimal.name
            }
            else {
                vc.initCache = [selectedAnimal.name, selectedAnimal.image, selectedAnimal.scientificName, selectedAnimal.tidbit]
            }
        }
    }
}

class AnimalCell: UICollectionViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var image: UIImageView!
    var gradient = Color.gradientLayer()
    
    func configureForAnimal(animal: Animal) {
        name.text = animal.name
        name.numberOfLines = 0
        
        //Get Image
        Animal.getThumbnailImage(animal.image, image: &image)
    }
}

class NetworkErrorMessage { // maybe just change this to be a method inside HttpHandler
    let errorMsg: String = "Oops! Something went wrong"
    var width: CGFloat!
    var height: CGFloat!
    var viewController: UIViewController!
    
    init(width: CGFloat, height: CGFloat, viewController: UIViewController){
        self.width = UIScreen.mainScreen().bounds.size.width as CGFloat!
        self.height = 100.0
        self.viewController = viewController
    }
}
