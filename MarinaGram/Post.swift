//
//  Post.swift
//  MarinaGram
//
//  Created by Yousup Lee on 4/16/15.
//  Copyright (c) 2015 Yousup Lee. All rights reserved.
//

import UIKit

@objc
class Post {
    let title: String
    let id: Int
    let content: String
    let date: String
    let image: String?
    
    private struct postStruct {
        static let baseUrl: String = "http://Aquide-vitxpp35pz.elasticbeanstalk.com/api/post/"
//        static let baseUrl: String = "http://localhost:3000/api/post/"
        static var postCache: Array<Post> = []
        static var postImageCache: NSCache = NSCache()
    }
    
    init(title: String, id: Int, content: String, date: String, image: String){
        self.title = title
        self.id = id
        self.content = content
        self.date = date
        self.image = image
    }
    
    class func retrievePosts(lastPost: Post?) -> Array<Post> {
        var posts: Array<Post> = []
        var jsonDictionary: NSDictionary!
        if (postStruct.postCache.isEmpty || lastPost != nil) {
            if (lastPost == nil){
                jsonDictionary = HttpHandler.getJSON(postStruct.baseUrl, query: "0")
            }
            else {
                jsonDictionary = HttpHandler.getJSON(postStruct.baseUrl, query: String(lastPost!.id))
            }
            if jsonDictionary != nil {
                for value in jsonDictionary!.allValues {
                    posts.append(Post(title: value["title"]! as! String, id: value["id"] as! Int, content: value["content"] as! String, date: value["date"] as! String, image: "Posts/" + (value["image"]! as! String)))
                }
                postStruct.postCache = posts
            }
            else {
                return []
            }
        }
        else {
            return postStruct.postCache
        }
        return posts
    }
    
    class func getPostCount() -> Int {
        var numPosts = 0
        var jsonDictionary: NSDictionary!
        jsonDictionary = HttpHandler.getJSON(postStruct.baseUrl, query: "postcount")
        if jsonDictionary != nil {
            for value in jsonDictionary!.allValues {
                numPosts = value as! Int
            }
        }
        return numPosts
    }
    
    class func getPostImage(imageName: String, inout image: UIImageView!) {
        image.image = postStruct.postImageCache.objectForKey(imageName) as? UIImage
        if ((image.image) != nil) {
            // Nothing to do
        }
        else {
            HttpHandler.getImageAsync("http://Aquide-vitxpp35pz.elasticbeanstalk.com/image/post/", query: imageName, image: &image, cache: &postStruct.postImageCache)
        }
    }
}
