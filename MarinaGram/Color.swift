//
//  Color.swift
//  Aquide
//
//  Created by Yousup Lee on 12/13/14.
//  Copyright (c) 2014 Yousup Lee. All rights reserved.
//

import UIKit
import Foundation

class Color: UIColor {
    class func blue3() -> UIColor {
        return Color.UIColorFromHex(0x2B75D6)
    }
    
    class func blue2() -> UIColor {
        return Color.UIColorFromHex(0x3280BF)
    }
    
    class func blue1() -> UIColor {
        return Color.UIColorFromHex(0x3B97E3)
    }
    
    class func blue4() -> UIColor {
        return Color.UIColorFromHex(0x4CA8F5)
    }
    
    class func blue5() -> UIColor {
        return Color.UIColorFromHex(0x2B6FA6)
    }
    
    class func gradientLayer() -> CAGradientLayer {
        let gl = CAGradientLayer()
        gl.colors = [UIColor.clearColor().CGColor, UIColor.blackColor().CGColor]
        gl.locations = [0.4, 1.0]
        return gl
    }
    
    class func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0) -> UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
}
