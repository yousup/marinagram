//
//  AquariaTableViewController.swift
//  MarinaGram
//
//  Created by Yousup Lee on 3/21/15.
//  Copyright (c) 2015 Yousup Lee. All rights reserved.
//

import UIKit

class AquariaTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var aquariaTableView: UITableView!
    var aquaria: Array<Aquarium>!
    
    struct TableView {
        struct AquariumCellIdentifiers {
            static let AquariumCell = "AquariumCell"
            static let ComingSoonCell = "ComingSoonCell"
        }
    }
    
    override func viewDidLoad() {
        aquaria = Aquarium.allAquariums()
        
        aquariaTableView.estimatedRowHeight = 72.0
        aquariaTableView.rowHeight = UITableViewAutomaticDimension
        
        if aquaria.count % 2 == 0 {
            aquariaTableView.backgroundColor = Color.blue5()
        }
        else {
            aquariaTableView.backgroundColor = Color.blue2()
        }
        
        aquariaTableView.reloadData()
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(animated: Bool) {
        self.title = "Aquaria"
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aquaria.count + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        if (indexPath.row == aquaria.count) {
            let cell = tableView.dequeueReusableCellWithIdentifier(TableView.AquariumCellIdentifiers.ComingSoonCell, forIndexPath: indexPath) as! ComingSoonCell
            if (indexPath.row%2 == 0){
                cell.contentView.backgroundColor = Color.blue5()
            }
            else {
                cell.contentView.backgroundColor = Color.blue2()
            }
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCellWithIdentifier(TableView.AquariumCellIdentifiers.AquariumCell, forIndexPath: indexPath) as! AquariumCell
            cell.configureForAquarium(aquaria[indexPath.row])
            if (indexPath.row%2 == 0){
                cell.contentView.backgroundColor = Color.blue5()
            }
            else {
                cell.contentView.backgroundColor = Color.blue2()
            }
            return cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!){
        if segue.identifier == "tableToAquarium" {
            let vc = segue.destinationViewController as! AquariumViewController
//            vc.hidesBottomBarWhenPushed = true
            var selectedAquarium: Aquarium!
            let path = aquariaTableView.indexPathForSelectedRow()!
            selectedAquarium = aquaria[path.row]
            self.navigationItem.title = nil
//            vc.animals = Animal.animalsForAquarium(selectedAquarium.name)
            if let i = vc.name {
                vc.name.text = selectedAquarium.name
            }
            else {
                vc.initCache = [selectedAquarium.name]
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

class AquariumCell: UITableViewCell {
    @IBOutlet weak var details: UILabel!
    
    func configureForAquarium(aquarium: Aquarium) {
        details.text = aquarium.name + " // " + aquarium.location
        details.textColor = UIColor.whiteColor()
    }
}

class ComingSoonCell: UITableViewCell {
    @IBOutlet weak var comingSoon: UILabel!
}