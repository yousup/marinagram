//
//  Aquarium.swift
//  Aquide
//
//  Created by Yousup Lee on 11/15/14.
//  Copyright (c) 2014 Yousup Lee. All rights reserved.
//

import UIKit

@objc
class Aquarium {
    let name: String
    let location: String
    let image: String
    let about: String
    private struct aquariumStruct {
        static var cache: Array<Aquarium> = []
    }
    
    init(name: String, location: String, image: String, about: String){
        self.name = name
        self.location = location
        self.image = image
        self.about = about
    }
    
    class func allAquariums() -> Array<Aquarium> {
        return [
            Aquarium(name: "Shedd Aquarium", location: "Chicago, IL, USA", image: "sheddaquarium.jpg", about: "Nothing here yet")
//            Aquarium(name: "Georgia Aquarium", location: "Atlanta, GA, USA", image: "georgiaaquarium.jpg", about: "asdf"),
//            Aquarium(name: "Monterey Bay Aquarium", location: "Monterey, CA, USA", image: "montereybayaquarium.jpg", about: "asdf")
        ]
    }
    
}
