//
//  HttpHandler.swift
//  Aquide
//
//  Created by Yousup Lee on 12/3/14.
//  Copyright (c) 2014 Yousup Lee. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore
import SystemConfiguration

class HttpHandler {
    class func getJSON(preUrl: String, query: String) -> NSDictionary? {
        // Fix query by replacing all whitespace with underscores
        var newName: String = query.stringByReplacingOccurrencesOfString(" ", withString: "_", options: NSStringCompareOptions.LiteralSearch, range: nil)
        // define url path and the whole request
        let urlPath: String = preUrl + newName
        var url: NSURL = NSURL(string: urlPath)!
        var request = NSMutableURLRequest(URL: url, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData, timeoutInterval: 10.0)
        
        // Ascertain that the request is a "get" method and provide authorization token
        request.HTTPMethod = "GET"
        request.setValue("Token 4f3e80b1746f85da2cf7652e60a0833d", forHTTPHeaderField: "Authorization")
        
        // create variables to store the response and any potential errors
        var response: AutoreleasingUnsafeMutablePointer<NSURLResponse?> = nil
        var error: NSError?
        var activityIndicator = UIActivityIndicatorView()
        
        activityIndicator.startAnimating()
        var jsonResult: NSDictionary = [:]
        
        // fire off the request and organize response data into a readable string/json/whatever
        var responseData = NSURLConnection.sendSynchronousRequest(request,returningResponse: response, error: &error) as NSData!
        if error != nil || responseData == nil{
            return nil
        }
        else {
            jsonResult = NSJSONSerialization.JSONObjectWithData(responseData, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSDictionary
        }
        
        activityIndicator.stopAnimating()
        //        activityIndicator.removeActivityIndicator()
        return jsonResult
    }
    
    class func getImageAsync(preUrl: String, query: String, inout image: UIImageView!, inout cache: NSCache) {
        // Fix query by replacing all whitespace with underscores
        var newName: String = query.stringByReplacingOccurrencesOfString(" ", withString: "_", options: NSStringCompareOptions.LiteralSearch, range: nil).stringByReplacingOccurrencesOfString("'", withString: "_", options: NSStringCompareOptions.LiteralSearch, range: nil).stringByReplacingOccurrencesOfString("-", withString: "_", options: NSStringCompareOptions.LiteralSearch, range: nil)
        
        // define url path and the whole request
        let urlPath: String = preUrl + newName
        var url: NSURL = NSURL(string: urlPath)!
        var request = NSMutableURLRequest(URL: url)
        
        // Ascertain that the request is a "get" method and provide authorization token
        request.HTTPMethod = "GET"
        request.setValue("Token 4f3e80b1746f85da2cf7652e60a0833d", forHTTPHeaderField: "Authorization")
        
        // Creating NSOperationQueue to which the handler block is dispatched when the request completes or failed
        var queue: NSOperationQueue = NSOperationQueue()
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        NSURLConnection.sendAsynchronousRequest(request, queue: queue, completionHandler:{(response:NSURLResponse!, responseData:NSData!, error: NSError!) -> Void in
            if error != nil
            {
            }
            else
            {
                //Converting data to Image
                image.image = UIImage(data: responseData)!
                CATransaction.flush()
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                cache.setObject(image.image!, forKey: query)
            }
        })
    }
    
    class func getImageAsync(preUrl: String, query: String, inout image: UIImageView!) {
        // Fix query by replacing all whitespace with underscores
        var newName: String = query.stringByReplacingOccurrencesOfString(" ", withString: "_", options: NSStringCompareOptions.LiteralSearch, range: nil).stringByReplacingOccurrencesOfString("'", withString: "_", options: NSStringCompareOptions.LiteralSearch, range: nil).stringByReplacingOccurrencesOfString("-", withString: "_", options: NSStringCompareOptions.LiteralSearch, range: nil)
        
        // define url path and the whole request
        let urlPath: String = preUrl + newName
        var url: NSURL = NSURL(string: urlPath)!
        var request = NSMutableURLRequest(URL: url)
        
        // Ascertain that the request is a "get" method and provide authorization token
        request.HTTPMethod = "GET"
        request.setValue("Token 4f3e80b1746f85da2cf7652e60a0833d", forHTTPHeaderField: "Authorization")
        
        // Creating NSOperationQueue to which the handler block is dispatched when the request completes or failed
        var queue: NSOperationQueue = NSOperationQueue()
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        NSURLConnection.sendAsynchronousRequest(request, queue: queue, completionHandler:{(response:NSURLResponse!, responseData:NSData!, error: NSError!) -> Void in
            if error != nil
            {
            }
            else
            {
                //Converting data to Image
                image.image = UIImage(data: responseData)!
                CATransaction.flush()
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            }
        })
    }
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0)).takeRetainedValue()
        }
        
        var flags: SCNetworkReachabilityFlags = 0
        if SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) == 0 {
            return false
        }
        
        let isReachable = (flags & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        return (isReachable && !needsConnection) ? true : false
    }

    class func outputNetworkError() -> UIView {
        var errorBox: UIView!
        errorBox.frame = CGRectMake(0,0,UIScreen.mainScreen().bounds.size.width as CGFloat!,100)
        return errorBox
    }
    
}