//
//  AnimalViewController.swift
//  MarinaGram
//
//  Created by Yousup Lee on 3/24/15.
//  Copyright (c) 2015 Yousup Lee. All rights reserved.
//

import UIKit

class AnimalViewController: UIViewController {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var scientificName: UILabel!
    @IBOutlet weak var tidbit: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var ImageTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottom: NSLayoutConstraint!
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    
    var initCache = []
    var imagePortraitHeight: CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Get Image
        Animal.getAnimalImage(initCache[1] as! String, image: &image)
        //Set name label
        name.text = initCache[0] as? String
        //Set scientific name
        scientificName.text = initCache[2] as? String
        //Set tidbit
        tidbit.text = initCache[3] as? String
        //Set title
        self.title = initCache[0] as? String
        
        imagePortraitHeight = image.frame.height
    }
    
    override func viewWillLayoutSubviews() {
        // Resize the collection cells on rotation
        if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation))
        {
            ImageTopConstraint.constant = -32
            bottom.constant = -22
            imageHeight.constant = imagePortraitHeight
        }
        
        if(UIDeviceOrientationIsPortrait(UIDevice.currentDevice().orientation))
        {
            ImageTopConstraint.constant = -64
            bottom.constant = -54
            imageHeight.constant = 0
        }
        super.viewWillLayoutSubviews()
    }
}
