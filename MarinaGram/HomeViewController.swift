//
//  ViewController.swift
//  MarinaGram
//
//  Created by Yousup Lee on 3/21/15.
//  Copyright (c) 2015 Yousup Lee. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var postsTableView: UITableView!
    var posts: Array<Post>!
    var numPosts = 0
    
    struct TableView {
        struct PostCellIdentifiers {
            static let PostCell = "PostCell"
            static let LoadMoreCell = "LoadMoreCell"
        }
    }

    override func viewDidLoad() {        
        // Do any additional setup after loading the view, typically from a nib.
        posts = Post.retrievePosts(nil)
        if let a = posts {
            errorLabel.hidden = true
            refreshButton.hidden = true
            numPosts = Post.getPostCount()
        }
        else {
            errorLabel.hidden = false
            refreshButton.hidden = false
        }
        postsTableView.estimatedRowHeight = 80.0
        postsTableView.rowHeight = UITableViewAutomaticDimension
        
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        postsTableView.reloadData()
        super.viewWillLayoutSubviews()
    }

    @IBAction func refresh() {
        viewDidLoad()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if numPosts != posts.count {
            return posts.count + 1
        }
        else {
            return posts.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if (indexPath.row == posts.count && numPosts != posts.count) {
            let cell = tableView.dequeueReusableCellWithIdentifier(TableView.PostCellIdentifiers.LoadMoreCell, forIndexPath: indexPath) as! LoadMoreCell
            cell.loadMore.text = "Load More"
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCellWithIdentifier(TableView.PostCellIdentifiers.PostCell, forIndexPath: indexPath) as! PostCell
            cell.configureForPost(posts[indexPath.row])
            return cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if (indexPath.row == posts.count){
            // Load more posts
            if posts == nil || posts.count == 0 {
                posts = Post.retrievePosts(nil)
            }
            else {
                posts! += Post.retrievePosts(posts[posts.count - 1])
            }
            let y = postsTableView.contentOffset.y
            self.postsTableView.reloadData()
            
            dispatch_async(dispatch_get_main_queue()) {
                self.postsTableView.setContentOffset(CGPointMake(0, y), animated: false)
            }
        
        }
    }
}

class PostCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var content: UITextView!
    @IBOutlet weak var picture: UIImageView!
    
    func configureForPost(post: Post) {
        title.text = post.title
        title.numberOfLines = 0
        title.textColor = UIColor.whiteColor()
        content?.text = post.content
        content?.textColor = UIColor.whiteColor()
        dispatch_async(dispatch_get_main_queue()) {
            Post.getPostImage(post.image!, image: &self.picture)
        }
        date.text = post.date
    }
}

class LoadMoreCell: UITableViewCell {
    @IBOutlet weak var loadMore: UILabel!
}